#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\gtc\type_ptr.hpp";
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "Helpers.cpp";
#include "Texture.cpp";


using namespace std;

namespace Mesh
{
	class Mesh
	{
	protected:
		template <typename T>
		void addData(vector<T> data, vector<T> * thisDta)
		{
			for (int i = 0; i < data.size(); i++)
			{
				thisDta->push_back(data[i]);
			}

		}

	public:
		unsigned int vertexBufferHandle;
		unsigned int indexBufferHandle;

		Texture::Texture texture;
		bool usingTexture;

		vector<float> data;
		vector<unsigned int> positions;

		GLuint dataHandle;
		GLuint positionHandle;

		int count;


		int dataOffset;

		int normalsOffset;

		int textureCoordOffset;

		Mesh(string path, GLuint program)
		{
			usingTexture = true;
			texture = Texture::Texture();

			loadObj2(path);
			Utility::stringReplace(path, ".obj", ".jpg");
			if (!Utility::fileExist(path.c_str()))
			{
				usingTexture = false;
			}

			if (usingTexture)
			{
				texture.loadTexture(path);
				texture.registerTextureUniform(program, "textureData");
			}
		}

		Mesh()
		{

		}

		void addVertexData(vector<float> data)
		{
			addData(data, &this->data);
		}

		void addPositionData(vector<unsigned int> data)
		{
			addData(data, &this->positions);
		}

		string getRelativePath(string path)
		{
			string strFilename = __FILE__;
			string::size_type pos = strFilename.find_last_of("\\/");
			strFilename = strFilename.substr(0, pos);

			strFilename += path;

			return strFilename;
		}

		virtual void draw()
		{

			glBindBuffer(GL_ARRAY_BUFFER, dataHandle);
		//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, positionHandle);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);

			if (usingTexture)
			{
				texture.activateTexture();
			}
			// Vertices
		//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)dataOffset);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)dataOffset);

			// Texture coordinates
		//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *)textureCoordOffset);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(6*sizeof(float)));

			// Normals
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(3 * sizeof(float)));


			glDrawArrays(GL_TRIANGLES, 0, count);
		//	glDrawElements(GL_TRIANGLES, positions.size(), GL_UNSIGNED_INT, 0);

			if (usingTexture)
			{
				texture.deactiveTexture();
			}

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glDisableVertexAttribArray(2);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		void generateArrayBuffer()
		{
			glGenBuffers(1, &dataHandle);

			glBindBuffer(GL_ARRAY_BUFFER, dataHandle);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)* data.size(), &data.front(), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
		}

		void generateElementBuffer()
		{
			glGenBuffers(1, &positionHandle);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, positionHandle);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* positions.size(), &positions.front(), GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		void loadProcessedObj(string filename)
		{
			ifstream in(filename, ios::in);
			int inputInt;
			float inputFloat;
			char inputChar;

			bool readingPositions = false;
			bool readingData = false;
			bool readingOffsets = true;
			
			int offsets[3];
			int offsetPointer = 0;

			while (!in.eof())
			{
				if (readingOffsets)
				{
					if (offsetPointer >= 3)
					{
						readingOffsets = false;
						readingPositions = true;
						continue;
					}

					in >> inputInt;
					offsets[offsetPointer] = inputInt;

					offsetPointer++;
					in.get(inputChar);
					continue;
				}
				else if (readingPositions)
				{
					in >> inputInt;
					in.get(inputChar);

					positions.push_back(inputInt);

					if (inputChar == '\n')
					{
						readingPositions = false;
						readingData = true;
						continue;

					}

					continue;
				}
				else if (readingData)
				{
					in >> inputFloat;
					in.get(inputChar);

					data.push_back(inputFloat);
					continue;
				}

				in.get(inputChar);
			}

			dataOffset = offsets[0];
			normalsOffset = offsets[1];
			textureCoordOffset = offsets[2];

			generateArrayBuffer();
			generateElementBuffer();
		}

		void loadObj2(string filename)
		{
			string processedFile = filename;
			Utility::stringReplace(processedFile, ".obj", ".mesh");


			ofstream out(processedFile, ios::out);


			ifstream in(filename, ios::in);
			vector<glm::vec4> vertices;
			vector<glm::vec3> objectNormals;
			vector<vector<GLuint>> elements;
			vector<pair<float, float>> textureCoords;


			if (!in) { cerr << "Cannot open " << filename << endl; exit(1); }
			parseFile(in, vertices, elements, textureCoords, objectNormals);


			dataOffset = 0;
			out << dataOffset << endl;
			out << (elements.size() * 3 * sizeof(float)) << endl; // normals offset
			out << ((elements.size() * 3 * sizeof(float)) + (elements.size() * 3 * sizeof(float))) << endl; // texture coord offset

			//addPositionData(elements);
			count = elements.size() * 3;

			for (int i = 0; i < elements.size(); i++)
			{
				pair<float, float> v1Tex;
				pair<float, float> v2Tex;
				pair<float, float> v3Tex;
				if (elements[i][1] != -1)
				{
					v1Tex = textureCoords[elements[i][1]];
					v2Tex = textureCoords[elements[i][4]];
					v3Tex = textureCoords[elements[i][7]];
				}
				else
				{
					v1Tex = pair<float, float>(0.0f, 0.0f);
					v2Tex = pair<float, float>(0.0f, 1.0f);
					v3Tex = pair<float, float>(1.0f, 0.0f);
				}
				glm::vec3 v1 = glm::vec3(vertices[elements[i][0]]);
				glm::vec3 v2 = glm::vec3(vertices[elements[i][3]]);
				glm::vec3 v3 = glm::vec3(vertices[elements[i][6]]);
				
				pushVec3ToData(v1);
				//pushVec3ToData(normals[elements[i][0]]);
				pushVec3ToData(objectNormals[elements[i][2]]);
				data.push_back(v1Tex.first);
				data.push_back(v1Tex.second);
			//	data.push_back(0.0f);
			//	data.push_back(0.0f);


				pushVec3ToData(v2);
				//pushVec3ToData(normals[elements[i][3]]);
				pushVec3ToData(objectNormals[elements[i][5]]);
				data.push_back(v2Tex.first);
				data.push_back(v2Tex.second);
				//data.push_back(1.0f);
				//data.push_back(0.0f);

				pushVec3ToData(v3);
				//pushVec3ToData(normals[elements[i][6]]);
				pushVec3ToData(objectNormals[elements[i][8]]);
				data.push_back(v3Tex.first);
				data.push_back(v3Tex.second);
				//data.push_back(0.0f);
			//	data.push_back(1.0f);

				
				
			}


			addVertexData(data);

			in.close();
			out.close();

			generateArrayBuffer();

		}

		void loadObj(string filename)
		{
			string processedFile = filename;
			Utility::stringReplace(processedFile, ".obj", ".mesh");

			if (Utility::fileExist(processedFile.c_str()))
			{
				loadProcessedObj(processedFile);
				return;
			}
			
			cout << processedFile << endl;

			ofstream out(processedFile, ios::out);

			bool vtPresent = false;

			ifstream in(filename, ios::in);
			vector<glm::vec4> vertices;
			vector<GLuint> elements;


			if (!in) { cerr << "Cannot open " << filename << endl; exit(1); }


			string line;
			while (getline(in, line))
			{
				if (line.substr(0, 2) == "v ")
				{
					istringstream s(line.substr(2));
					glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0f;
					vertices.push_back(v);
				}
				else if (line.substr(0, 2) == "f ")
				{
					istringstream s(line.substr(2));
					GLushort a, b, c;
					char skip;
					int skipNorm;


					s >> a;

					s >> skip;

					if (vtPresent == true)
					{
						s >> skipNorm;
					}

					s >> skip;
					s >> skipNorm;

					s >> b;

					s >> skip;

					if (vtPresent == true)
					{
						s >> skipNorm;
					}

					s >> skip;
					s >> skipNorm;

					s >> c;



					a--; b--; c--;
					elements.push_back(a); elements.push_back(b); elements.push_back(c);
				}
				else if (line.substr(0, 2) == "vt")
				{
					vtPresent = true;
				}
				else if (line[0] == '#')
				{ /* ignoring this line */
				}
				else
				{ /* ignoring this line */
				}
			}

			vector<glm::vec3> normals(vertices.size());

			for (int i = 0; i < elements.size(); i = i + 3) {
				GLuint ia = elements[i];
				GLuint ib = elements[i + 1];
				GLuint ic = elements[i + 2];


				glm::vec3 normal = glm::normalize(glm::cross(
					glm::vec3(vertices[ib]) - glm::vec3(vertices[ia]),
					glm::vec3(vertices[ic]) - glm::vec3(vertices[ia])));
				normals[ia] = normals[ib] = normals[ic] = normal;
			}
			dataOffset = 0;
			out << dataOffset << endl;
			out << (vertices.size() * 4 * sizeof(float)) << endl; // normals offset
			out << ((vertices.size() * 4 * sizeof(float)) + (normals.size() * 3 * sizeof(float))) << endl; // texture coord offset

			addPositionData(elements);
			for (int i = 0; i < elements.size(); i++)
			{
				out << elements[i];
				
				if (i + 1 == elements.size())
				{
					out << endl;
				}
				else
				{
					out << " ";
				}
			}



			vector<float> data;

			for (int i = 0; i < vertices.size(); i++)
			{
				data.push_back(vertices[i].x);
				data.push_back(vertices[i].y);
				data.push_back(vertices[i].z);
				data.push_back(vertices[i].w);

				out << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << " " << vertices[i].w;
				if (i + 1 == vertices.size())
				{
					out << endl;
				}
				else
				{
					out << " ";
				}

			}

			normalsOffset = data.size() * sizeof(float);

			for (int i = 0; i < normals.size(); i++)
			{
				data.push_back(normals[i].x);
				data.push_back(normals[i].y);
				data.push_back(normals[i].z);

				out << normals[i].x << " " << normals[i].y << " " << normals[i].z;
				if (i + 1 == normals.size())
				{
					out << endl;
				}
				else
				{
					out << " ";
				}
			}

			textureCoordOffset = data.size() * sizeof(float);

			for (int i = 0; i < vertices.size(); i = i + 4)
			{
				data.push_back(0.0f);
				data.push_back(0.0f);

				data.push_back(1.0f);
				data.push_back(0.0f);

				data.push_back(1.0f);
				data.push_back(1.0f);

				data.push_back(0.0f);
				data.push_back(1.0f);

				out << 0.0f << " " << 0.0f << " " << 1.0f << " " << 0.0f << " " << 1.0f << " " << 1.0f << " " << 0.0f << " " << 1.0f;
				if (i + 4 >= vertices.size())
				{
					break;
				}
				else
				{
					out << " ";
				}

			}

			addVertexData(data);
			
			in.close();
			out.close();

			generateArrayBuffer();
			generateElementBuffer();
		}

		void printToFile(string name)
		{
			ofstream out(name, ios::out);
			out << dataOffset << " " << normalsOffset << " " << textureCoordOffset << endl;
			
			for (int i = 0; i < data.size(); i++)
			{
				out << data[i] << endl;
			}
			for (int i = 0; i < positions.size(); i++)
			{
				out << positions[i] << endl;
			}


			out.close();
		}

		void pushVec3ToData(glm::vec3 &dta)
		{
			data.push_back(dta.x);
			data.push_back(dta.y);
			data.push_back(dta.z);
		}

		void parseFile(ifstream &in, vector<glm::vec4> &vertices, vector<vector<GLuint>> &elements = vector<vector<GLuint>>(), vector<pair<float, float>> &texCoords = vector<pair<float, float>>(), vector<glm::vec3> &normals = vector<glm::vec3>())
		{
			bool vtPresent = false;
			string line;
			while (getline(in, line))
			{
				if (line.substr(0, 2) == "v ")
				{
					istringstream s(line.substr(2));
					glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0f;
					vertices.push_back(v);
				}
				else if (line.substr(0, 2) == "f ")
				{
					istringstream s(line.substr(2));
					GLuint a, b, c;
					GLuint d, e, f;
					GLuint g, h, j;
					char skip;
					int skipNorm;

					s >> a;

					s >> skip;

					if (vtPresent == true)
					{
						s >> d;
					}

					s >> skip;
					s >> g;

					s >> b;

					s >> skip;

					if (vtPresent == true)
					{
						s >> e;
					}

					s >> skip;
					s >> h;

					s >> c;
					s >> skip;
					if (vtPresent == true)
					{
						s >> f;
					}

					s >> skip;
					s >> j;

					a--; b--; c--;
					if (vtPresent)
					{
						d--; e--; f--;
					}
					else
					{
						d = e = f = -1;
					}
					g--; h--; j--;

					vector<GLuint> arr({ a, d, g, b, e, h, c, f, j });
					elements.push_back(arr);
				}
				else if (line.substr(0, 3) == "vn ")
				{
					istringstream s(line.substr(3));
					float first;
					float second;
					float third;
					s >> first; s >> second; s >> third;
					normals.push_back(glm::vec3(first, second, third));

				}
				else if (line.substr(0, 3) == "vt ")
				{
					istringstream s(line.substr(3));
					float first;
					float second;
					s >> first; s >> second;
					texCoords.push_back(pair<float, float>(first, second));

					vtPresent = true;
				}
				else if (line[0] == '#')
				{ /* ignoring this line */
				}
				else
				{ /* ignoring this line */
				}
			}
		}

	};



}