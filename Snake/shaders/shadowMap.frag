#version 450


void main()
{
	vec3 pos = gl_FragCoord.xyz / gl_FragCoord.w; //-1 to 1
	pos = pos * 0.5 + 0.5;

	float depth = pos.z;
	//gl_FragDepth = gl_FragCoord.z;
	//gl_FragColor = vec4(gl_FragCoord.z, gl_FragCoord.z * gl_FragCoord.z, 0.0, 0.0);

	gl_FragColor = vec4(gl_FragCoord.z, depth * depth, 0.0, 0.0);
}