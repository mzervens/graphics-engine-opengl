#version 450

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 texCoord;

out vec2 textureCoordinate;


void main()
{	
	gl_Position = position;
	textureCoordinate = texCoord;
}
