#pragma once;

#include <iostream>
#include <string>
#include <fstream>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";

using namespace std;

namespace Utility
{
	static int lastRenderTime = 0;
	static int framesRendered = 0;
	static bool firstRenderTime = true;

	static void stringReplace(std::string& str, const std::string& from, const std::string& to)
	{
		if (from.empty())
		{
			return;
		}
			
		size_t start_pos = 0;
		while ((start_pos = str.find(from, start_pos)) != std::string::npos) 
		{
			str.replace(start_pos, from.length(), to);
			start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
		}
	}

	static bool fileExist(const char *fileName)
	{
		ifstream infile(fileName);
		return infile.good();
	}


	static int updateRenderTime()
	{
		if (firstRenderTime)
		{
			lastRenderTime = glutGet(GLUT_ELAPSED_TIME);
			firstRenderTime = false;
			return 0;
		}

		int currTime = glutGet(GLUT_ELAPSED_TIME);
		int oldTime = lastRenderTime;

		lastRenderTime = currTime;
		int difference = currTime - oldTime;

		return currTime - oldTime;

	}

	static void printGLMMat4(glm::mat4 mat)
	{
		cout << mat[0].x << " " << mat[1].x << " " << mat[2].x << " " << mat[3].x << endl;
		cout << mat[0].y << " " << mat[1].y << " " << mat[2].y << " " << mat[3].y << endl;
		cout << mat[0].z << " " << mat[1].z << " " << mat[2].z << " " << mat[3].z << endl;
		cout << mat[0].w << " " << mat[1].w << " " << mat[2].w << " " << mat[3].w << endl;
	}
	
}