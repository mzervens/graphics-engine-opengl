#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp";
#include "KeyboardInput.cpp";

using namespace std;

namespace Core
{
	class Camera
	{
	public:
		glm::vec3 cameraPos;
		glm::vec3 cameraUp;
		glm::vec3 cameraDirection;
		glm::vec3 cameraRight;
		glm::vec3 lookPosition;
		glm::vec3 worldUp;

		float moveSpeed;
		float sensitivity;

		float YAW;
		float PITCH;

		glm::mat4 view;
		GLuint viewHandle;
		string viewName;

		glm::mat3 viewNormal;
		GLuint viewNormalHandle;
		string viewNormalName;


		GLuint cameraPosHandle;
		string cameraPosName;

		Camera()
		{
			cameraPos = glm::vec3(0.0f, 0.0f, 2.5f);
			worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
			cameraDirection = glm::vec3(0.0f, 0.0f, -1.0f);

			moveSpeed = 0.2f;
			sensitivity = 0.05f;

			YAW = PITCH = 0.0f;
			updateCameraVectors();
			createViewMatrix();
		}

		void setPosition(glm::vec3 newPos)
		{
			cameraPos = newPos;
			createViewMatrix();
		}

		void setYAWPITCH(float YAW, float PITCH)
		{
			this->YAW = YAW;
			this->PITCH = PITCH;

			updateCameraVectors();
			createViewMatrix();
		}


		void setCameraView(glm::mat4 view)
		{
			this->view = view;
		}

		void orientateCamera(int mouseX, int mouseY)
		{
			YAW += (mouseX * sensitivity);
			PITCH += (mouseY * sensitivity);

			if (PITCH > 89.0f)
			{
				PITCH = 89.0f;
			}
			if (PITCH < -89.0f)
			{
				PITCH = -89.0f;
			}

			normalizeDegrees(YAW);

			updateCameraVectors();
			createViewMatrix();
		}



		void moveCamera(unsigned char key, int mouseX, int mouseY)
		{
			if (key == 'w' || key == 'W')
			{
				glm::vec3 moved = cameraDirection * moveSpeed;
				cameraPos += moved;
			}
			else if (key == 's' || key == 'S')
			{
				glm::vec3 moved = cameraDirection * moveSpeed;
				cameraPos -= moved;
			}
			else if (key == 'a' || key == 'A')
			{
				glm::vec3 moved = cameraRight * moveSpeed;
				cameraPos -= moved;
			}
			else if (key == 'd' || key == 'D')
			{
				glm::vec3 moved = cameraRight * moveSpeed;
				cameraPos += moved;
			}
			printMtx(view);

			createViewMatrix();
		}

		void createViewMatrix()
		{
			view = glm::lookAt(cameraPos, cameraPos + cameraDirection, cameraUp);
			glm::mat3 normal = glm::mat3();
			normal[0] = glm::vec3(view[0]);
			normal[1] = glm::vec3(view[1]);
			normal[2] = glm::vec3(view[2]);

			viewNormal = normal;
		}


		void updateCameraVectors()
		{
			// Calculate the new Front vector
			glm::vec3 direction;
			direction.x = cos(glm::radians(YAW)) * cos(glm::radians(PITCH));
			direction.y = sin(glm::radians(PITCH));
			direction.z = sin(glm::radians(YAW)) * cos(glm::radians(PITCH));
			
			cameraDirection = glm::normalize(direction);
			cameraRight = glm::normalize(glm::cross(cameraDirection, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
			cameraUp = glm::normalize(glm::cross(cameraRight, cameraDirection));

		}

		void normalizeDegrees(float &deg)
		{
			while (deg > 360.0f)
			{
				deg -= 360.0f;
			}

			while (deg < -360.0f)
			{
				deg += 360.0f;
			}
		}


		void registerViewUniform(GLuint program, string name)
		{
			viewName = name;
			glUseProgram(program);

			viewHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(viewHandle, 1, GL_FALSE, glm::value_ptr(view));

			glUseProgram(0);
		}

		void updateViewUniform(GLuint program)
		{
			glUseProgram(program);
			viewHandle = glGetUniformLocation(program, viewName.c_str());
			glUniformMatrix4fv(viewHandle, 1, GL_FALSE, glm::value_ptr(view));
			glUseProgram(0);
		}

		void registerViewNormalUniform(GLuint program, string name)
		{
			viewNormalName = name;
			glUseProgram(program);

			viewNormalHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix3fv(viewNormalHandle, 1, GL_FALSE, glm::value_ptr(viewNormal));

			glUseProgram(0);
		}

		void updateViewNormalUniform(GLuint program)
		{
			glUseProgram(program);
			viewNormalHandle = glGetUniformLocation(program, viewNormalName.c_str());
			glUniformMatrix3fv(viewNormalHandle, 1, GL_FALSE, glm::value_ptr(viewNormal));
			glUseProgram(0);
		}

		void registerCameraPositionUniform(GLuint program, string name)
		{
			cameraPosName = name;
			glUseProgram(program);
			cameraPosHandle = glGetUniformLocation(program, name.c_str());
			glUniform3fv(cameraPosHandle, 1, glm::value_ptr(cameraPos));
			glUseProgram(0);

		}

		void updateCameraPositionUniform(GLuint program)
		{
			glUseProgram(program);
			cameraPosHandle = glGetUniformLocation(program, cameraPosName.c_str());
			glUniform3fv(cameraPosHandle, 1, glm::value_ptr(cameraPos));
			glUseProgram(0);

		}

		void printPosition()
		{
			cout << endl;
			cout << cameraPos.x << " " << cameraPos.y << " " << cameraPos.z << endl;
		}

		void printRotation()
		{
			cout << endl;
			cout << YAW << " " << PITCH << endl;
		}

		void printDir()
		{
			cout << cameraDirection.x << " " << cameraDirection.y << " " << cameraDirection.z << endl;
		}

		void printMtx(glm::mat4 mat)
		{
			cout << endl;

			cout << " x " << mat[0].x << " x " << mat[1].x << " x " << mat[2].x << " x " << mat[3].x << endl;
			cout << " y " << mat[0].y << " y " << mat[1].y << " y " << mat[2].y << " y " << mat[3].y << endl;
			cout << " z " << mat[0].z << " z " << mat[1].z << " z " << mat[2].z << " z " << mat[3].z << endl;
			cout << " w " << mat[0].w << " w " << mat[1].w << " w " << mat[2].w << " w " << mat[3].w << endl << endl;
		}

	};

}