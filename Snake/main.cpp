#pragma once;

#include <iostream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"
#include "ShaderLoader.cpp";
#include "Mesh.cpp";
#include "Projection.cpp";
#include "RenderOptions.cpp";
#include "WindowOptions.cpp"
#include "Camera.cpp";
#include "KeyboardInput.cpp"
#include "Light.cpp";
#include "Rendering.cpp";
#include "Texture.cpp";
#include "ModelTransform.cpp";
#include "DynamicMesh.cpp";

// PhysX
//#include <PxPhysicsAPI.h>
//#include "PhysX.cpp";
//using namespace physx;

// Bullet
#include "Bullet.cpp";


GLuint shaderProgram;
GLuint lightShader;
GLuint textureToScreenShader;
GLuint shadowmapShader;
Core::Projection projection;
Core::Camera camera;

Window::WindowOptions windowOpts;

Mesh::Mesh plain;
Mesh::Mesh boxTop;
Mesh::Mesh boxBot;
Mesh::Mesh boxFront;
Mesh::Mesh boxBack;
Mesh::Mesh boxLeft;
Mesh::Mesh boxRight;

Mesh::Mesh cubeXL;
Mesh::Mesh cubeL;
Mesh::Mesh cubeM;
Mesh::Mesh cubeS;

Mesh::Mesh SphereXL;

Mesh::Mesh lamp;

Mesh::Mesh bridge;
Mesh::Mesh watertower;


Mesh::DynamicMesh dynamicBox;
Mesh::DynamicMesh dynamicSphere;
Mesh::DynamicMesh dynamicSuzane;
Mesh::DynamicMesh dynamicWallCube;
vector<Mesh::DynamicMesh> addedDynamicSpheres = vector<Mesh::DynamicMesh>();
vector<Mesh::DynamicMesh> addedWallCubes = vector<Mesh::DynamicMesh>();

Light::Light sceneLight;

Rendering::Rendering rendering;

Texture::Texture loadingscreen;

Transform::ModelTransform* MTransf;

void renderShadowMap();

RenderingPhys::SimpleBullet* physics = new RenderingPhys::SimpleBullet();

int renderTimeAccumulationMS = 0;
int minUpdateTimeMS = 17;
int timesUpdated = 0;
void render()
{
	renderShadowMap();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, rendering.shadowmapTexture);
	
	

	plain.draw();
	bridge.draw();
	watertower.draw();

	cubeXL.draw();
	cubeL.draw();
	cubeM.draw();
	cubeS.draw();

	SphereXL.draw();

	MTransf->transform = dynamicBox.getTransform();
	MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
	dynamicBox.draw();

	MTransf->transform = dynamicSphere.getTransform();
	MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
	dynamicSphere.draw();

	MTransf->transform = dynamicSuzane.getTransform();
	MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
	dynamicSuzane.draw();


	MTransf->transform = dynamicWallCube.getTransform();
	MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
	dynamicWallCube.draw();

	for (int i = 0; i < addedDynamicSpheres.size(); i++)
	{
		MTransf->transform = addedDynamicSpheres[i].getTransform();


		MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
		addedDynamicSpheres[i].draw();
	}

	for (int i = 0; i < addedWallCubes.size(); i++)
	{
		MTransf->transform = addedWallCubes[i].getTransform();
		MTransf->updateModelTransformUniform(shaderProgram, "", shaderProgram);
		addedWallCubes[i].draw(shaderProgram);
	}

	MTransf->setTransformToIdentity(shaderProgram);

	//glUseProgram(lightShader);
	//lamp.draw();

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
	glutSwapBuffers();


	// Physics update
	int difference = Utility::updateRenderTime();
	renderTimeAccumulationMS += difference;

	if (renderTimeAccumulationMS >= minUpdateTimeMS)
	{
		btScalar timeStep = ((float) renderTimeAccumulationMS / 1000);
		int maxSubSteps = ceil(renderTimeAccumulationMS / ((1.0f / 60.0f) * 1000));
	//	cout << "rendertime " << timeStep << " substeps " << maxSubSteps << endl;
		physics->simulate(timeStep, maxSubSteps);
		renderTimeAccumulationMS = 0;
		timesUpdated++;
	}

	glutPostRedisplay();
}

void drawScene(GLuint program = 0)
{
	plain.draw();
	bridge.draw();
	watertower.draw();

	cubeXL.draw();
	cubeL.draw();
	cubeM.draw();
	cubeS.draw();
	SphereXL.draw();
	lamp.draw();

	if (program)
	{
		MTransf->transform = dynamicBox.getTransform();
		MTransf->updateModelTransformUniform(program, "", program);
		dynamicBox.draw(program);

		MTransf->transform = dynamicSphere.getTransform();
		MTransf->updateModelTransformUniform(program, "", program);
		dynamicSphere.draw(program);

		MTransf->transform = dynamicSuzane.getTransform();
		MTransf->updateModelTransformUniform(program, "", program);
		dynamicSuzane.draw(program);

		MTransf->transform = dynamicWallCube.getTransform();
		MTransf->updateModelTransformUniform(program, "", program);
		dynamicWallCube.draw(program);

		for (int i = 0; i < addedDynamicSpheres.size(); i++)
		{
			MTransf->transform = addedDynamicSpheres[i].getTransform();
			MTransf->updateModelTransformUniform(program, "", program);
			addedDynamicSpheres[i].draw(program);
		}

		for (int i = 0; i < addedWallCubes.size(); i++)
		{
			MTransf->transform = addedWallCubes[i].getTransform();
			MTransf->updateModelTransformUniform(program, "", program);
			addedWallCubes[i].draw(program);
		}


		MTransf->setTransformToIdentity(program);
	}
	else
	{
		dynamicBox.draw();
		dynamicSphere.draw();

		for (int i = 0; i < addedDynamicSpheres.size(); i++)
		{
			addedDynamicSpheres[i].draw();
		}
	}
}


void renderShadowMap()
{
	glViewport(0, 0, 4096, 4096);
	rendering.bindToFrameBufferShadowMap();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClear(GL_DEPTH_BUFFER_BIT);

	glUseProgram(shadowmapShader);
	glCullFace(GL_FRONT);
	drawScene(shadowmapShader);

	glUseProgram(0);
	rendering.bindToScreenFrameBuffer();
	glViewport(0, 0, 1920, 1080);

	glCullFace(GL_BACK);
}


void viewShadowMap()
{
	renderShadowMap(); 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(textureToScreenShader);

	rendering.renderTextureToScreen(rendering.shadowmapTexture);

	glUseProgram(0);
	glutSwapBuffers();
	glutPostRedisplay();

}



void renderLoadingScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(textureToScreenShader);

	rendering.renderTextureToScreen(loadingscreen);

	glUseProgram(0);
	glutSwapBuffers();
}


void movementFunction(unsigned char key, int mouseX, int mouseY)
{
	camera.moveCamera(key, mouseX, mouseY);
	camera.updateViewUniform(shaderProgram);
	camera.updateViewNormalUniform(shaderProgram);
	camera.updateCameraPositionUniform(shaderProgram);
	camera.updateViewUniform(lightShader);
}


void escapeLoop(unsigned char key, int mouseX, int mouseY)
{
	glutLeaveMainLoop();
}

int oldX = 0;
int oldY = 0;
bool firstInput = true;
void mouseMovement(int x, int y)
{
	if (x == oldX && y == oldY)
	{
		return;
	}

	if (firstInput)
	{
		firstInput = false;
		GLint centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
		GLint centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;
		oldX = centerX;
		oldY = centerY;
		windowOpts.moveCursor(oldX, oldY);
		return;
	}

	x = x - oldX;
	y = oldY - y;

	GLint centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
	GLint centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

	oldX = centerX;
	oldY = centerY;

	windowOpts.moveCursor(oldX, oldY);

	camera.orientateCamera(x, y);
	camera.updateViewUniform(shaderProgram);
	camera.updateViewNormalUniform(shaderProgram);
	camera.updateViewUniform(lightShader);
}

void printPos(unsigned char key, int x, int y)
{
	camera.printPosition();
}
void printOrient(unsigned char key, int x, int y)
{
	camera.printRotation();
}


void setCurrPosToLight(unsigned char key, int x, int y)
{
	sceneLight.position = camera.cameraPos;
	sceneLight.registerLightPosition(shaderProgram, "lightPosition");
}

void stepSimulation(unsigned char key, int x, int y)
{
	cout << "times updated " << timesUpdated << endl;
	return;

	physics->simulate(1.0f / 60.0f);
	dynamicBox.printTransform();
}

void addDynamicSphere(unsigned char key, int x, int y)
{
	glm::mat4 modelTransf = glm::mat4(1.0f);
	
	modelTransf[3].x = camera.cameraPos.x;
	modelTransf[3].y = camera.cameraPos.y;
	modelTransf[3].z = camera.cameraPos.z;

	glm::vec3 veloc = camera.cameraDirection;

	Mesh::DynamicMesh dynSphere = Mesh::DynamicMesh(&dynamicSphere, shaderProgram, modelTransf, veloc * 20.0f);
	addedDynamicSpheres.push_back(dynSphere);
}

void addDynamicBox(unsigned char key, int x, int y)
{
	glm::mat4 modelTransf = glm::mat4(1.0f);

	modelTransf[3].x = camera.cameraPos.x;
	modelTransf[3].y = camera.cameraPos.y;
	modelTransf[3].z = camera.cameraPos.z;

	glm::vec3 veloc = camera.cameraDirection;

	Mesh::DynamicMesh dynSphere = Mesh::DynamicMesh(&dynamicWallCube, shaderProgram, modelTransf, veloc * 60.0f);
	addedWallCubes.push_back(dynSphere);
}

//13.679 11.3345 -30.309
void createBoxStack(glm::vec3 position)
{
	Mesh::DynamicMesh mesh;
	glm::mat4 modelMat;
	glm::vec3 veloc = glm::vec3(0.0f, 0.0f, 0.0f);
	float aproxDim = 0.525f;

	for (int z = 0; z < 8; z++)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int n = 0; n < 3; n++)
			{
				modelMat = glm::mat4(1.0f);
				modelMat[3].x = position.x + (aproxDim * i);
				modelMat[3].y = position.y + ((aproxDim) * z);
				modelMat[3].z = position.z + (aproxDim * n);

				mesh = Mesh::DynamicMesh(&dynamicWallCube, shaderProgram, modelMat, veloc);
				addedWallCubes.push_back(mesh);

			}

		}
	}



}

void addDynamicBoxStack(unsigned char key, int x, int y)
{
	glm::vec3 position = camera.cameraPos;
	createBoxStack(position);
}

void registerPerspectiveProj(int width, int height)
{
	projection = Core::Projection(65.0f, (float)width / height, 0.1f, 10000.0f);
	projection.registerPerspectiveUniform(shaderProgram, "transformMatrix");
	projection.registerPerspectiveUniform(lightShader, "transformMatrix");
}

void toggleFullScreen(unsigned char key, int x, int y)
{
	windowOpts.toggleFullScreen();
	windowOpts.initDimensionParameters();
	registerPerspectiveProj(windowOpts.width, windowOpts.height);
}


int main(int argc, char * argv[])
{
	physics->createPlane();
	//physics->addSphere(3.0f, 0.0f, 20.0f, 0.0f, 5.0f);
	//physics.simulateTest();

	int defWidth = 1920;
	int defHeight = 1080;

	// Initializes GLUT (OpenGL Utility Toolkit)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH | GLUT_STENCIL | GLUT_ACCUM | GLUT_RGBA);
	glutInitWindowSize(defWidth, defHeight);
	glutInitWindowPosition(0, 0);

	GLuint windowHandle = glutCreateWindow("V01d engine");
	
	glewInit();
	if (glewIsSupported("GL_VERSION_4_5"))
	{
		cout << " GLEW Version is 4.5\n ";
	}
	else
	{
		cout << "GLEW 4.5 not supported\n ";
	}


	Core::ShaderLoader shaderLoader = Core::ShaderLoader();
	shaderProgram = shaderLoader.createShaderProgram("../Snake/shaders/vertex_engine.vert", "../Snake/shaders/fragment_engine.frag");
	lightShader = shaderLoader.createShaderProgram("../Snake/shaders/lightsource.vert", "../Snake/shaders/lightsource.frag");
	textureToScreenShader = shaderLoader.createShaderProgram("../Snake/shaders/textureToScreen.vert", "../Snake/shaders/textureToScreen.frag");
	shadowmapShader = shaderLoader.createShaderProgram("../Snake/shaders/shadowMap.vert", "../Snake/shaders/shadowMap.frag");

	rendering.init();

	registerPerspectiveProj(defWidth, defHeight);

	MTransf = new Transform::ModelTransform();
	MTransf->registerModelTransformUniform(shaderProgram, "modelMatrix");
	MTransf->registerModelTransformForNormalsUniform(shaderProgram, "modelMatForNormal");
	MTransf->registerModelTransformUniform(shadowmapShader, "modelMatrix");
	MTransf->registerModelTransformUniform(lightShader, "modelMatrix");
	

	loadingscreen.loadTexture("../Snake/textures/loading.png");
	loadingscreen.registerTextureUniform(textureToScreenShader, "textureData");

	renderLoadingScreen();

	Rendering::RenderOptions renderOpts = Rendering::RenderOptions();
	windowOpts = Window::WindowOptions();
	
	
	renderOpts.enableDepthTesting(GL_LESS);
	renderOpts.enableFaceCulling();
	renderOpts.setClearingColor(0.3f, 0.3f, 0.6f, 1.0f);

	windowOpts.hideCursor();

	camera = Core::Camera();
	camera.registerViewUniform(shaderProgram, "viewMatrix");
	camera.registerViewNormalUniform(shaderProgram, "viewNormalMatrix");
	camera.registerCameraPositionUniform(shaderProgram, "cameraPosition");
	camera.registerViewUniform(lightShader, "viewMatrix");
	
	

	vector<char> movementKeys = { 'w', 's', 'a', 'd',
		'W', 'S', 'A', 'D' };

	Input::keyboard.registerCallbackMultipleKeys(movementFunction, movementKeys);
	Input::keyboard.registerCallback(escapeLoop, 27);
	Input::keyboard.registerCallback(printPos, 'x');
	Input::keyboard.registerCallback(printOrient, 'z');
	Input::keyboard.registerCallback(toggleFullScreen, 'f');
	Input::keyboard.registerCallback(setCurrPosToLight, 'c');
	Input::keyboard.registerCallback(stepSimulation, 'p');
	Input::keyboard.registerCallback(addDynamicSphere, ' ');
	Input::keyboard.registerCallback(addDynamicBox, 'b');
	Input::keyboard.registerCallback(addDynamicBoxStack, 'B');

	plain = Mesh::Mesh("../Snake/meshes/CornelBox/plain/plain.obj", shaderProgram);

	boxBack = Mesh::Mesh("../Snake/meshes/CornelBox/box_back/box_back.obj", shaderProgram);
	boxFront = Mesh::Mesh("../Snake/meshes/CornelBox/box_front/box_front.obj", shaderProgram);
	boxBot = Mesh::Mesh("../Snake/meshes/CornelBox/box_bot/box_bot.obj", shaderProgram);
	boxTop = Mesh::Mesh("../Snake/meshes/CornelBox/box_top/box_top.obj", shaderProgram);
	boxLeft = Mesh::Mesh("../Snake/meshes/CornelBox/box_left/box_left.obj", shaderProgram);
	boxRight = Mesh::Mesh("../Snake/meshes/CornelBox/box_right/box_right.obj", shaderProgram);

	cubeXL = Mesh::Mesh("../Snake/meshes/CornelBox/cube_xl/cube_xl.obj", shaderProgram);
	cubeL = Mesh::Mesh("../Snake/meshes/CornelBox/cube_l/cube_l.obj", shaderProgram);
	cubeM = Mesh::Mesh("../Snake/meshes/CornelBox/cube_m/cube_m.obj", shaderProgram);
	cubeS = Mesh::Mesh("../Snake/meshes/CornelBox/cube_s/cube_s.obj", shaderProgram);

	SphereXL = Mesh::Mesh("../Snake/meshes/CornelBox/sphere_xl/sphere_xl.obj", lightShader);

	lamp = Mesh::Mesh("../Snake/meshes/CornelBox/lamp/lamp.obj", lightShader);

	bridge = Mesh::Mesh("../Snake/meshes/bridge/Bridges.obj", shaderProgram);
	watertower = Mesh::Mesh("../Snake/meshes/watertower/watertower.obj", shaderProgram);

	dynamicBox = Mesh::DynamicMesh("../Snake/meshes/fallingCube/fallingCube.obj", shaderProgram, physics);
	dynamicSphere = Mesh::DynamicMesh("../Snake/meshes/dynamicSphere/dynamicSphere.obj", shaderProgram, physics);
	dynamicSuzane = Mesh::DynamicMesh("../Snake/meshes/suzanneOrigin/suzanneOrigin.obj", shaderProgram, physics);
	dynamicWallCube = Mesh::DynamicMesh("../Snake/meshes/cubeForWall/cubeForWall.obj", shaderProgram, physics);
//	camera.setPosition(glm::vec3(6.0f, 1.42521f, 4.95038f));
//	camera.setYAWPITCH(220.7f, -0.55002f);
		
	//camera.setPosition(glm::vec3(8.23314f, 14.6105f, 7.59798f));
	camera.setPosition(glm::vec3(12.195f, 8.17331f, -17.0758f));
	camera.setYAWPITCH(225.949f, -48.2506f);


	camera.updateViewUniform(shaderProgram);
	camera.updateViewUniform(lightShader);
	
	//sceneLight = Light::Light(glm::vec3(-5.53685f, 14.0789f, 5.79565f), glm::vec3(1.0f, 1.0f, 1.0f), Light::LightType::POINT);
	sceneLight = Light::Light(glm::vec3(146.282f, -114.374f, -46.5429f), glm::vec3(1.0f, 1.0f, 1.0f), Light::LightType::DIRECTIONAL);
	sceneLight.registerLightColor(shaderProgram, "lightColor");
	sceneLight.registerLightPosition(shaderProgram, "lightPosition");
	sceneLight.registerAmbientLightStrength(shaderProgram, "ambientStrength");
	sceneLight.registerLightType(shaderProgram, "lightType");
	sceneLight.registerLightDirection(shaderProgram, "lightDirection");

//	sceneLight.initLightProjectionMatrix();
	sceneLight.initLightViewMatrix();

//	sceneLight.setLightView(camera.view);
	sceneLight.setLightProjection(projection.perspective);
	sceneLight.registerLightViewProjMatrix(shadowmapShader, "PV");
	sceneLight.registerLightViewProjMatrix(shaderProgram, "lightPV");

//	camera.setCameraView(sceneLight.lightView);
//	camera.updateViewUniform(shaderProgram);

	//Texture::Texture().registerTextureUniform(shaderProgram, "shadowmap");
	glUseProgram(shaderProgram);
	GLuint texLoc = glGetUniformLocation(shaderProgram, "shadowmap");
	glUniform1i(texLoc, 1);
	glUseProgram(0);

	//13.679 11.3345 -30.309
	createBoxStack(glm::vec3(13.679f, 0.0145f, -30.309f));


	glutPassiveMotionFunc(mouseMovement);
	glutKeyboardFunc(Input::keyInputFunction);
	glutDisplayFunc(render);
	glutMainLoop();

	return 0;


}

