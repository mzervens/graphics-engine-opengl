#pragma once


#include <iostream>
// PhysX
#include <PxPhysicsAPI.h>
#include <foundation\PxMat33.h>

#ifdef _DEBUG
#pragma comment(lib, "PhysX3DEBUG_x86.lib")
#pragma comment(lib, "PhysX3CommonDEBUG_x86.lib")
#pragma comment(lib, "PhysX3ExtensionsDEBUG.lib")
#else
#pragma comment(lib, "PhysX3_x86.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#endif
using namespace physx;
using namespace std;


namespace RenderingPhys
{
	class SimplePhysx
	{
	public:

		PxDefaultErrorCallback gDefaultErrorCallback;
		PxDefaultAllocator gDefaultAllocatorCallback;
		PxFoundation * gFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);

		PxPhysics * gPhysicsAPI = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale());

		PxScene* gScene = NULL;
		

		PxMaterial * mMaterial = NULL;

		PxRigidStatic * ground = NULL;
		PxRigidDynamic * fallingObj = NULL;

		void simulatePhysix(double time)
		{
			gScene->simulate(time);
			gScene->fetchResults(true);
		}


		void shutDownPhysix()
		{
			gScene->release();
			gFoundation->release();
			gPhysicsAPI->release();

		}

		void init()
		{
			// Physix init
			if (gPhysicsAPI == NULL)
			{
				cout << "Error initializing physix";
			}

			PxInitExtensions(*gPhysicsAPI);

			// pos 3.35075 0.235325 0.376806
			PxSceneDesc sceneDescr(gPhysicsAPI->getTolerancesScale());

			sceneDescr.gravity = PxVec3(0.0f, -9.807f, 0.0f);
			sceneDescr.cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
			sceneDescr.filterShader = PxDefaultSimulationFilterShader;

			gScene = gPhysicsAPI->createScene(sceneDescr);

			mMaterial = gPhysicsAPI->createMaterial(0.5f, 0.5f, 0.5f);


			PxTransform planePos = PxTransform(PxVec3(0.0f, 0.235325f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
			ground = gPhysicsAPI->createRigidStatic(planePos);
			ground->createShape(PxPlaneGeometry(), *mMaterial);


			PxTransform fallingObjPos = PxTransform(PxVec3(0.0f, 10.0f, 0.0f));
			PxBoxGeometry fallingObjGeo(PxVec3(0.5f, 0.5f, 0.5f));

			fallingObj = PxCreateDynamic(*gPhysicsAPI, fallingObjPos, fallingObjGeo, *mMaterial, 1.0f);

			gScene->addActor(*ground);
			gScene->addActor(*fallingObj);
			//

		}

		void simulate()
		{
			double step = 1.0 / 60.0;
			cout << "==== Physix simulation in progress ====" << endl;

			for (int i = 0; i < 300; i++)
			{
				if (gScene)
				{
					simulatePhysix(step);
					PxVec3 pos = fallingObj->getGlobalPose().p;
					cout << "Object position: " << pos.x << " " << pos.y << " " << pos.z << endl;
				}

			}

			cout << endl;
		}

	};

}