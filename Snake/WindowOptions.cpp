#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"

using namespace std;

namespace Window
{
	class WindowOptions
	{
	public:

		bool fullScreen;
		int width;
		int height;

		WindowOptions()
		{
			width = height = -1;
			fullScreen = false;
		}

		void hideCursor()
		{
			glutSetCursor(GLUT_CURSOR_NONE);
		}

		void moveCursor(int x, int y)
		{
			glutWarpPointer(x, y);
		}
		void initDimensionParameters()
		{
			width = glutGet(GLUT_SCREEN_WIDTH);
			height = glutGet(GLUT_SCREEN_HEIGHT);
		}
		void toggleFullScreen()
		{
			if (width == -1 || height == -1)
			{
				initDimensionParameters();
			}


			if (fullScreen)
			{
				glutReshapeWindow(width, height);
				glutPositionWindow(0, 0);
			}
			else
			{
				glutFullScreen();
			}


			fullScreen = !fullScreen;


		}


	};

}