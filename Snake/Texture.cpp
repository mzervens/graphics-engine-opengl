#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp";
#include "ImageLoading.cpp";
#include "lib\SOIL\SOIL.h";

namespace Texture
{
	class Texture
	{
	public:
		GLuint textureHandle;
		GLuint textureUniformHandle;

		void activateTexture()
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureHandle);
		}
		void deactiveTexture()
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		Texture(const Texture &obj)
		{
			this->textureHandle = obj.textureHandle;
			this->textureUniformHandle = obj.textureUniformHandle;
		}

		Texture()
		{

		}

		void loadTexture(string path)
		{
			int width;
			int height;
			unsigned char * imageData = Image::loadImage(path, width, height);

			cout << "width " << width << "height " << height << endl;

			glGenTextures(1, &textureHandle);
			glBindTexture(GL_TEXTURE_2D, textureHandle);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//glTexImage2D()
			//glTexImage2D()
			glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);


			glBindTexture(GL_TEXTURE_2D, 0);

			SOIL_free_image_data(imageData);

		}

		void registerTextureUniform(GLuint program, string name)
		{
			glUseProgram(program);

			textureUniformHandle = glGetAttribLocation(program, name.c_str());
			glUseProgram(0);
		}

	};


}
