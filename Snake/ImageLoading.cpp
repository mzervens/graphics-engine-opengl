#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp";
#include "lib\SOIL\SOIL.h";


using namespace std;

namespace Image
{
	static unsigned char* loadImage(string path, int &width, int &height)
	{
		return SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
	}

}