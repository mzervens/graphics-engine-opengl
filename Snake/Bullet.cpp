#pragma once
#include <iostream>
#include <btBulletDynamicsCommon.h>
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\gtc\type_ptr.hpp";

using namespace std;

namespace RenderingPhys
{
	class SimpleBullet
	{
	public:
		btDefaultCollisionConfiguration* bltDefColConf;
		btCollisionDispatcher* bltColDispatcher;
		btBroadphaseInterface* bltBrdPhaseIntfce;
		btSequentialImpulseConstraintSolver* bltSolver;

		btDiscreteDynamicsWorld* bltDynamicsWorld;

		btAlignedObjectArray<btRigidBody*> rigidBodies;


		SimpleBullet()
		{
			bltDefColConf = new btDefaultCollisionConfiguration();
			bltColDispatcher = new btCollisionDispatcher(bltDefColConf);
			bltBrdPhaseIntfce = new btDbvtBroadphase();
			bltSolver = new btSequentialImpulseConstraintSolver();

			bltDynamicsWorld = new btDiscreteDynamicsWorld(bltColDispatcher, bltBrdPhaseIntfce, bltSolver, bltDefColConf);
			setGravity();


		}

		~SimpleBullet()
		{
			// removes rigidbodies from the simulation world and deletes them
			for (int i = bltDynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
			{
				btCollisionObject* bltColObj = bltDynamicsWorld->getCollisionObjectArray()[i];
				btRigidBody* bltRigBdy = btRigidBody::upcast(bltColObj);

				if (bltRigBdy && bltRigBdy->getMotionState())
				{
					delete bltRigBdy->getMotionState();
				}
				bltDynamicsWorld->removeCollisionObject(bltColObj);

			}


			// Delete the collision shapes
			for (int i = 0; i < rigidBodies.size(); i++)
			{
				btCollisionShape* collisionShape = rigidBodies[i]->getCollisionShape();
				delete collisionShape;
				delete rigidBodies[i];
			}
			//

			delete bltDynamicsWorld;
			delete bltSolver;
			delete bltBrdPhaseIntfce;
			delete bltColDispatcher;
			delete bltDefColConf;

		}


		void setGravity(float x = 0.0f, float y = -9.8f, float z = 0.0f)
		{
			bltDynamicsWorld->setGravity(btVector3(x, y, z));
		}

		void createPlane()
		{
			btTransform transf;
			transf.setIdentity();
			transf.setOrigin(btVector3(0.0f, 0.0f, 0.0f));

			btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0.0f, 1.0f, 0.0f), 0.0f);
			btMotionState* planeState = new btDefaultMotionState(transf);

			btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(0.0f, planeState, plane);

			btRigidBody* rigidBodyPlane = new btRigidBody(rigidBodyInfo);

			bltDynamicsWorld->addRigidBody(rigidBodyPlane);
			rigidBodies.push_back(rigidBodyPlane);
		}


		void addSphere(float radius, float x, float y, float z, float mass)
		{
			btTransform transf;
			transf.setIdentity();
			transf.setOrigin(btVector3(x, y, z));

			btSphereShape* sphere = new btSphereShape(radius); 
			btMotionState* sphereState = new btDefaultMotionState(transf);

			btVector3 localInertia = btVector3(0, 0, 0);
			if (mass > 0)
			{
				sphere->calculateLocalInertia(mass, localInertia);
			}

			btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(mass, sphereState, sphere, localInertia);

			btRigidBody* rigidBodySphere = new btRigidBody(rigidBodyInfo);

			bltDynamicsWorld->addRigidBody(rigidBodySphere);
			rigidBodies.push_back(rigidBodySphere);

		}

		int addConvexHullRigidDynamic(btCollisionShape* colShape)
		{
			int id = rigidBodies.size();
			btTransform transf;
			transf.setIdentity();

			btMotionState* motionState = new btDefaultMotionState(transf);
			btScalar mass = 10.0f;
			btVector3 inertia(0, 0, 0);

			colShape->calculateLocalInertia(mass, inertia);

			btRigidBody::btRigidBodyConstructionInfo convexHullInfo = btRigidBody::btRigidBodyConstructionInfo(mass, motionState, colShape, inertia);

			btRigidBody* rigidBody = new btRigidBody(convexHullInfo);

			bltDynamicsWorld->addRigidBody(rigidBody);
			rigidBodies.push_back(rigidBody);
			return id;
		}

		int addConvexHullRigidDynamic(btCollisionShape* colShape, glm::vec3 origin)
		{
			int id = rigidBodies.size();
			btTransform transf;
			transf.setIdentity();
			transf.setOrigin(btVector3(origin.x, origin.y, origin.z));

			colShape->setMargin(0.001);

			btMotionState* motionState = new btDefaultMotionState(transf);
			btScalar mass = 10.0f;
			btVector3 inertia(0, 0, 0);

			colShape->calculateLocalInertia(mass, inertia);

			btRigidBody::btRigidBodyConstructionInfo convexHullInfo = btRigidBody::btRigidBodyConstructionInfo(mass, motionState, colShape, inertia);

			btRigidBody* rigidBody = new btRigidBody(convexHullInfo);

			bltDynamicsWorld->addRigidBody(rigidBody);
			rigidBodies.push_back(rigidBody);
			return id;
		}

		int addConvexHullRigidDynamic(btCollisionShape* colShape, glm::mat4 modelTransf, glm::vec3 linearVelocity = glm::vec3(0.0f, 0.0f, 0.0f))
		{
			int id = rigidBodies.size();
			btTransform transf;
			transf.setIdentity();
			transf.setOrigin(btVector3(modelTransf[3].x, modelTransf[3].y, modelTransf[3].z));

			btMotionState* motionState = new btDefaultMotionState(transf);
			btScalar mass = 10.0f;
			btVector3 inertia(0, 0, 0);

			colShape->calculateLocalInertia(mass, inertia);

			btRigidBody::btRigidBodyConstructionInfo convexHullInfo = btRigidBody::btRigidBodyConstructionInfo(mass, motionState, colShape, inertia);

			btRigidBody* rigidBody = new btRigidBody(convexHullInfo);
			btVector3 veloc = rigidBody->getLinearVelocity();
			rigidBody->setLinearVelocity(btVector3(linearVelocity.x, linearVelocity.y, linearVelocity.z));
			veloc = rigidBody->getLinearVelocity();


			bltDynamicsWorld->addRigidBody(rigidBody);
			rigidBodies.push_back(rigidBody);
			return id;
		}

		float* getRigidBodyTransform(int index)
		{
			btRigidBody* body = rigidBodies[index];
			btTransform transform;
			//body->getMotionState()->getWorldTransform(transform);
			

			
			float transfMat4[16];
			
			
			((btDefaultMotionState*)body->getMotionState())->m_graphicsWorldTrans.getOpenGLMatrix(transfMat4);
			//btMatrix3x3	rot; rot.setIdentity();
			//rot = ((btDefaultMotionState*)body->getMotionState())->m_graphicsWorldTrans.getBasis();
			
		/*	cout << "ROT MAT" << endl;
			for (int i = 0; i < 3; i++)
			{
				for (int n = 0; n < 3; n++)
				{
					cout << rot[i][n] << " ";
				}
				cout << endl;
			}
			cout << endl;
			*/
			//transform.getOpenGLMatrix(transfMat4);

			return transfMat4;
		}


		void simulate(btScalar timeFrame, int maxSubSteps = 1)
		{
			bltDynamicsWorld->stepSimulation(timeFrame, maxSubSteps);
		}


		void simulateTest()
		{
			for (int i = 0; i < 300; i++)
			{
				simulate(1.0f / 60.0f);

				btTransform transf;

				rigidBodies[1]->getMotionState()->getWorldTransform(transf);
				btVector3 pos = transf.getOrigin();
				
				cout << pos.x() << " " << pos.y() << " " << pos.z() << endl;
			}


		}

	};


}