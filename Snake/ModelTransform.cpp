#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\gtc\type_ptr.hpp";
#include "lib\glm\glm\gtc\matrix_transform.hpp"
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";

using namespace std;

namespace Transform
{
	class ModelTransform
	{
	public:
		glm::mat4 transform;
		glm::mat3 transfForNormals;
		GLuint transformHandle;
		GLuint transfForNormHandle;

		string transformName;
		string transfForNormName;

		ModelTransform()
		{
			transform = glm::mat4(1.0f);
		}

		void setTransformToIdentity(GLuint program = 0)
		{
			transform = glm::mat4(1.0f);
			transfForNormals = glm::mat3(1.0f);
			if (program)
			{
				updateModelTransformUniform(program);
			}
		}

		void createTransformForNormals()
		{
			transfForNormals = glm::mat3(glm::transpose(glm::inverse(transform)));
		}

		void translate()
		{
			glm::mat4 t = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
			transform = t;

			printGLMMatO(t);
		}

		void setTransformFromArray(float* arr, GLuint program = 0)
		{
			transform = glm::make_mat4(arr);
			if (program)
			{
				updateModelTransformUniform(program);
			}
		}

		void updateModelTransformUniform(GLuint program, string name = "", GLuint currProgram = 0, bool setForNormal = true)
		{
			if (name == "")
			{
				name = transformName;
			}

			glUseProgram(program);
			transformHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(transformHandle, 1, GL_FALSE, glm::value_ptr(transform));
			glUseProgram(currProgram);

			if (setForNormal)
			{
				createTransformForNormals();
				updateModelTransformForNormalsUniform(program, transfForNormName, currProgram);
			}
		}

		void registerModelTransformUniform(GLuint program, string name)
		{
			glUseProgram(program);

			transformHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(transformHandle, 1, GL_FALSE, glm::value_ptr(transform));

			glUseProgram(0);

			transformName = name;
		}

		void updateModelTransformForNormalsUniform(GLuint program, string name = "", GLuint currProgram = 0)
		{
			if (name == "")
			{
				name = transformName;
			}

			glUseProgram(program);
			transfForNormHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix3fv(transfForNormHandle, 1, GL_FALSE, glm::value_ptr(transfForNormals));
			glUseProgram(currProgram);
		}

		void registerModelTransformForNormalsUniform(GLuint program, string name)
		{
			glUseProgram(program);

			transfForNormHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix3fv(transfForNormHandle, 1, GL_FALSE, glm::value_ptr(transfForNormals));

			glUseProgram(0);

			transfForNormName = name;
		}

		void printGLMMatO(glm::mat4 mat)
		{
			cout << mat[0].x << " " << mat[1].x << " " << mat[2].x << " " << mat[3].x << endl;
			cout << mat[0].y << " " << mat[1].y << " " << mat[2].y << " " << mat[3].y << endl;
			cout << mat[0].z << " " << mat[1].z << " " << mat[2].z << " " << mat[3].z << endl;
			cout << mat[0].w << " " << mat[1].w << " " << mat[2].w << " " << mat[3].w << endl;
		}

	};

}