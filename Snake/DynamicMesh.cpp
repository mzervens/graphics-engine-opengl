#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\gtc\type_ptr.hpp";
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "Helpers.cpp";
#include "Texture.cpp";
#include "Mesh.cpp";
#include "ModelTransform.cpp";
#include <btBulletDynamicsCommon.h>
#include "Bullet.cpp";

namespace Mesh
{
	class DynamicMesh : public Mesh, Transform::ModelTransform
	{
	public:
		int rigidBodyId;
		RenderingPhys::SimpleBullet* physics;
		GLuint lastProgramUsed;
		float* transfMat;
		glm::mat4 modelTransf;
		btCollisionShape* collisionShp;
		glm::vec3 initialTranslation;

		DynamicMesh()
		{

		}

		DynamicMesh(string path, GLuint program, RenderingPhys::SimpleBullet* phsx) : Mesh(path, program), Transform::ModelTransform()
		{
			lastProgramUsed = program;
			modelTransf = glm::mat4(1.0f);
			initialTranslation = glm::vec3(0.0f, 0.0f, 0.0f);
			//this->registerModelTransformUniform(program, path);
			this->physics = phsx;

			vector<glm::vec4> vertices = vector<glm::vec4>();
			vector<vector<GLuint>> elements = vector<vector<GLuint>>();
			string bulletObjectPth = path;
			string initPositionFilePath = path;

			Utility::stringReplace(bulletObjectPth, ".obj", "_bt.obj");
			Utility::stringReplace(initPositionFilePath, ".obj", ".trans");
			ifstream bulletFile(bulletObjectPth, ios::in);
			ifstream initPosFile(initPositionFilePath, ios::in);
			if (initPosFile.good())
			{
				float posX, posY, posZ;
				initPosFile >> posX;
				initPosFile >> posY;
				initPosFile >> posZ;

				initialTranslation = glm::vec3(posX, posY, posZ);
				initPosFile.close();
			}

			

			this->parseFile(bulletFile, vertices, elements);
			bulletFile.close();

			btTriangleMesh* trimesh = new btTriangleMesh();
			btVector3 triangleV1 = btVector3();
			btVector3 triangleV2 = btVector3();
			btVector3 triangleV3 = btVector3();

			for (int i = 0; i < elements.size(); i++)
			{
				glmVec4TobtVec3(vertices[elements[i][0]], triangleV1);
				glmVec4TobtVec3(vertices[elements[i][3]], triangleV2);
				glmVec4TobtVec3(vertices[elements[i][6]], triangleV3);

				trimesh->addTriangle(triangleV1, triangleV2, triangleV3);
			}

			btCollisionShape* collisionShape = 0;
			collisionShape = new btConvexTriangleMeshShape(trimesh);
			collisionShp = collisionShape;

			rigidBodyId = physics->addConvexHullRigidDynamic(collisionShape, initialTranslation);
		}

		DynamicMesh(DynamicMesh* object, GLuint program, glm::mat4 modelTransform, glm::vec3 linVeloc = glm::vec3(0.0f, 0.0f, 0.0f)) : Transform::ModelTransform()
		{
			this->physics = object->physics;
			this->collisionShp = object->collisionShp;
			rigidBodyId = physics->addConvexHullRigidDynamic(collisionShp, modelTransform, linVeloc);
			this->initialTranslation = object->initialTranslation;

			this->texture = object->texture;
			this->usingTexture = object->usingTexture;


			this->dataHandle = object->dataHandle;
			this->dataOffset = object->dataOffset;
			this->count = object->count;

			this->lastProgramUsed = program;

		//	this->modelTransf = modelTransform;

		}

		glm::mat4 getTransform()
		{
			float* mat = physics->getRigidBodyTransform(rigidBodyId);
			glm::mat4 transform = glm::make_mat4(mat);
			return transform; //* modelTransf;
		}


		void printTransform()
		{
			float* mat = physics->getRigidBodyTransform(rigidBodyId);
			glm::mat4 transform = glm::make_mat4(mat);
			this->transform = transform;

			printGLMMat(this->transform);

			cout << endl;

			this->setTransformToIdentity();

			printGLMMat(this->transform);
			/*for (int i = 0; i < 16; i++)
			{
				cout << mat[i] << " ";
			}
			cout << endl;
			*/
		}

		void printGLMMat(glm::mat4 mat)
		{
			cout << mat[0].x << " " << mat[1].x << " " << mat[2].x << " " << mat[3].x << endl;
			cout << mat[0].y << " " << mat[1].y << " " << mat[2].y << " " << mat[3].y << endl;
			cout << mat[0].z << " " << mat[1].z << " " << mat[2].z << " " << mat[3].z << endl;
			cout << mat[0].w << " " << mat[1].w << " " << mat[2].w << " " << mat[3].w << endl;
		}

		void draw()
		{
		//	transfMat = physics->getRigidBodyTransform(rigidBodyId);
			
		//	this->updateModelTransformUniform(lastProgramUsed, "modelMatrix");
		//	this->setTransformFromArray(transfMat, lastProgramUsed);
			Mesh::draw();

		}

		void draw(GLuint program)
		{
			GLuint lastProgUsed = lastProgramUsed;
			lastProgramUsed = program;
			this->draw();

			lastProgramUsed = lastProgUsed;
		}


		void glmVec4TobtVec3(glm::vec4 glmVec4, btVector3 &btVec3)
		{
			btVec3.setX(glmVec4.x);
			btVec3.setY(glmVec4.y);
			btVec3.setZ(glmVec4.z);
		}


	};

}