#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"

using namespace std;

typedef void(*functionPointer)(unsigned char key, int mouseX, int mouseY);
//typedef function<void(unsigned char key, int mouseX, int mouseY) > functionPointer;

namespace Input
{

	class KeyboardInput
	{
		functionPointer keys[255];

	public:
		KeyboardInput()
		{
			for (int i = 0; i < 255; i++)
			{
				keys[i] = NULL;
			}
		}

		void registerCallbackMultipleKeys(functionPointer function, vector<char> keys)
		{
			for (int i = 0; i < keys.size(); i++)
			{
				this->keys[keys[i]] = function;
			}
		}
		
		void registerCallback(functionPointer function, char key)
		{
			keys[key] = function;
		}


		void keyFunction(unsigned char key, int mouseX, int mouseY)
		{
			if (keys[key] != NULL)
			{
				keys[key](key, mouseX, mouseY);
			}
		}



	};

	static KeyboardInput keyboard = KeyboardInput();
	static void keyInputFunction(unsigned char key, int mouseX, int mouseY)
	{
		keyboard.keyFunction(key, mouseX, mouseY);
	}
}