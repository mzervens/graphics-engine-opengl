#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"

using namespace std;

namespace Core
{
	class Projection
	{
	public:
		glm::mat4 perspective;
		glm::mat4 orthographic;

		GLuint perspectiveHandle;
		GLuint orthographicHandle;

		Projection()
		{

		}

		Projection(float fov, float aspectRatio, float zNear, float zFar)
		{
			perspective = glm::perspective(fov, aspectRatio, zNear, zFar);

			int width = glutGet(GLUT_SCREEN_WIDTH);
			int height = glutGet(GLUT_SCREEN_HEIGHT);

			orthographic = glm::ortho(0.0f, (float)width, (float)height, 0.0f);
		}

		void registerPerspectiveUniform(GLuint program, string name)
		{
			glUseProgram(program);

			perspectiveHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(perspectiveHandle, 1, GL_FALSE, glm::value_ptr(perspective));

			glUseProgram(0);
		}

		void registerOrthoUniform(GLuint program, string name)
		{
			glUseProgram(program);

			orthographicHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(orthographicHandle, 1, GL_FALSE, glm::value_ptr(orthographic));

			glUseProgram(0);
		}


	};


}