#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"

using namespace std;

namespace Rendering
{
	class RenderOptions
	{
	public:
		void enableDepthTesting(GLenum function)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);
			glDepthFunc(GL_LESS);
			glDepthRange(-1.0f, 1.0f);
		}
		void disableDepthTesting()
		{
			glDisable(GL_DEPTH_TEST);
		}

		void enableFaceCulling()
		{
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
		}
		void disableFaceCulling()
		{
			glDisable(GL_CULL_FACE);
		}

		void setClearingColor(float r, float g, float b, float a)
		{
			glClearColor(r, g, b, a);
		}


	};

}