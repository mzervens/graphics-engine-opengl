#pragma once

#include <iostream>
#include <vector>
#include <string>
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\gtc\type_ptr.hpp";
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\ext.hpp"

using namespace std;

namespace Light
{

	enum LightType
	{
		DIRECTIONAL = 0,
		POINT = 1

	};

	class Light
	{
	public:
		glm::vec3 direction; // If directional sun (ie the Sun)
		glm::vec3 position; // If point light (ie a lamp)
		glm::vec3 color;
		float ambientCoefficient;

		LightType lightType;

		GLuint positionHandle;
		GLuint colorHandle;
		GLuint ambientHandle;
		GLuint directionHandle;
		GLuint lightTypeHandle;


		glm::mat4 lightProjection;
		glm::mat4 lightView;
		glm::mat4 lightPV;

		Light()
		{

		}

		Light(glm::vec3 posOrDir, glm::vec3 color, LightType type)
		{
			if (type == LightType::POINT)
			{
				position = posOrDir;
				direction = glm::vec3(0.0f, 0.0f, 0.0f);
			}
			else
			{
				direction = posOrDir;
				position = glm::vec3(0.0f, 0.0f, 0.0f);
			}

			this->color = color;
			ambientCoefficient = 0.05;

			setLightType(type);
		}

		void initLightProjectionMatrix()
		{
			lightProjection = glm::ortho(0.0f, 1920.0f, 1080.0f, 0.0f, 0.1f, 10000.0f);
		}

		void initLightViewMatrix()
		{
			lightView = glm::lookAt(glm::vec3(-146.282f, 114.374f, 46.5429f), glm::vec3(146.282f, -114.374f, -46.5429f), glm::vec3(0.0f, 1.0f, 0.0f));
		}

		void setLightView(glm::mat4 view)
		{
			lightView = view;
		}

		void setLightProjection(glm::mat4 proj)
		{
			lightProjection = proj;
		}


		void setLightType(LightType type)
		{
			lightType = type;
		}

		void setAmbientLightStrength(float strength)
		{
			ambientCoefficient = strength;
		}


		void registerLightViewProjMatrix(GLuint program, string name)
		{
			lightPV = lightProjection * lightView;

			glUseProgram(program);
			GLuint PVHandle = glGetUniformLocation(program, name.c_str());
			glUniformMatrix4fv(PVHandle, 1, GL_FALSE, glm::value_ptr(lightPV));
			glUseProgram(0);
		}

		void registerLightPosition(GLuint program, string name)
		{
			glUseProgram(program);
			positionHandle = glGetUniformLocation(program, name.c_str());
			glUniform3fv(positionHandle, 1, glm::value_ptr(position));
			glUseProgram(0);
		}

		void registerLightColor(GLuint program, string name)
		{
			glUseProgram(program);
			colorHandle = glGetUniformLocation(program, name.c_str());
			glUniform3fv(colorHandle, 1, glm::value_ptr(color));
			glUseProgram(0);
		}

		void registerAmbientLightStrength(GLuint program, string name)
		{
			glUseProgram(program);
			ambientHandle = glGetUniformLocation(program, name.c_str());
			glUniform1fv(ambientHandle, 1, &ambientCoefficient);
			glUseProgram(0);
		}

		void registerLightDirection(GLuint program, string name)
		{
			glUseProgram(program);
			directionHandle = glGetUniformLocation(program, name.c_str());
			glUniform3fv(directionHandle, 1, glm::value_ptr(direction));
			glUseProgram(0);
		}

		void registerLightType(GLuint program, string name)
		{
			glUseProgram(program);
			lightTypeHandle = glGetUniformLocation(program, name.c_str());
			glUniform1i(lightTypeHandle, lightType);
			glUseProgram(0);
		}


	};
}