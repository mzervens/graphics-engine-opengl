#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp";
#include "Texture.cpp";

using namespace std;

namespace Rendering
{
	class Rendering
	{
		GLuint quadBuffer;

		GLuint frameBufferObject;

		void initQuadBuffer()
		{
			const float quadPositions[] =
			{
				-1.0f, -1.0f, 0.0f, 0.0f, 1.0f,
				1.0f, -1.0f, 0.0f, 1.0f, 1.0f,
				-1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
				-1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
				1.0f, -1.0f, 0.0f, 1.0f, 1.0f,
				1.0f, 1.0f, 0.0f, 1.0f, 0.0f

			};

			glGenBuffers(1, &quadBuffer);

			glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadPositions), quadPositions, GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		void initShadowmapping()
		{

			int width = glutGet(GLUT_SCREEN_WIDTH);
			int height = glutGet(GLUT_SCREEN_HEIGHT);

			GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };

			// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
			glGenFramebuffers(1, &frameBufferObject);
			glBindFramebuffer(GL_FRAMEBUFFER, frameBufferObject);

			// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
			glGenTextures(1, &shadowmapTexture);
			glBindTexture(GL_TEXTURE_2D, shadowmapTexture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, 4096, 4096, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 4096, 4096, 0, GL_RGBA, GL_FLOAT, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

			glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);


		//	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, shadowmapTexture, 0);

			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowmapTexture, 0);
		//	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, shadowmapTexture, 0);

		//	glDrawBuffer(GL_NONE); // No color buffer is drawn to.

			// Always check that our framebuffer is ok
			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				cout << "failed initializing framebuffer" << endl;
			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glBindTexture(GL_TEXTURE_2D, 0);

			/*glGenFramebuffers(1, &frameBufferObject);
			glGenTextures(1, &shadowmapTexture);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, shadowmapTexture);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


			glBindFramebuffer(GL_FRAMEBUFFER, frameBufferObject);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowmapTexture, 0);

			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);


			GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

			if (Status != GL_FRAMEBUFFER_COMPLETE) 
			{
				cout << "Failed initializing framebuffer object in class Rendering: " << Status << endl;
			}
			else
			{
				cout << "fbo statuss ok" << endl;
			}

			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			
			*/
		}

		void initCascadedShadowMaps(int cascades, float zNear, float zFar)
		{



		}

	public:
		GLuint shadowmapTexture;

		Rendering()
		{

		}

		void init()
		{
			initQuadBuffer();
			initShadowmapping();
		}

		void bindToFrameBufferShadowMap()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, frameBufferObject);
		}

		void bindToScreenFrameBuffer()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}



		void renderTextureToScreen(Texture::Texture &t)
		{
			glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);


			t.activateTexture();
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));


			glDrawArrays(GL_TRIANGLES, 0, 6);


			t.deactiveTexture();
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		void renderTextureToScreen(GLuint textureHandle)
		{
			glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);


			glBindTexture(GL_TEXTURE_2D, textureHandle);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));


			glDrawArrays(GL_TRIANGLES, 0, 6);


			glBindTexture(GL_TEXTURE_2D, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

		}

	};
}